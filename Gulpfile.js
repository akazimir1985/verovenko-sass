'use strict';

var path = require('path');
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var gulpIf = require('gulp-if');
var del = require('del');
var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');
var combiner = require('stream-combiner2').obj;
var uglify = require('gulp-uglify');
var cssnano = require('gulp-cssnano');
var gulplog = require('gulplog');
var browserSync = require('browser-sync').create();

var isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

gulp.task('sass-susy', function() {
    return combiner(
        gulp.src('client/style-susy/*.scss'),
        gulpIf(isDevelopment, sourcemaps.init()),
        sass({
            includePaths: ['node_modules/susy/sass', 'node_modules/font-awesome/scss']
        }),
        gulpIf(!isDevelopment, autoprefixer({
            browsers: ['last 2 versions', 'Firefox ESR', 'Opera 12.1', 'ie 9', 'android 2.3', 'android 4'],
            cascade: false
        })),
        gulpIf(!isDevelopment, cssnano()),
        gulpIf(isDevelopment, sourcemaps.write()),
        gulp.dest('public/css')
    ).on('error', notify.onError());

});

gulp.task('clean', function() {
    return del(['public/*', '!public/index.html', '!public/image', '!public/fonts']);
});

gulp.task('watch', function() {
    gulp.watch('style-susy/**/*.*', gulp.series('sass-susy'));
});

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy:'127.0.0.1:5000',
        port:5050
    });

    browserSync.watch(['public/**/*.*', 'views/**/*.*']).on('change', browserSync.reload);
});

gulp.task('build', gulp.series(
    'clean',
    gulp.parallel('sass-susy')
));

gulp.task('dev', gulp.series(
    'build',
    gulp.parallel('start-server', 'watch')
));